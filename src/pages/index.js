import MarketsPage from './Markets/Markets'
import MarketPages from './Market/Market'
import BasicLayout from './BasicLayout'

export default {MarketsPage, MarketPages, BasicLayout}