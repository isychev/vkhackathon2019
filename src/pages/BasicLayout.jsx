import React from 'react'
import PropTypes from 'prop-types'
import RouterRenderer from '../routers/RouterRenderer'

const BasicLayout = ({children, routes}) => {
  return (
    <div id="basicLayout" className="row h-100 no-gutters flex-md-nowrap">
      <div className="col h-100 bg-body">{
        <RouterRenderer siteMap={routes}/>
      }</div>
    </div>
  )
}

BasicLayout.propTypes = {
  children: PropTypes.node,
}
BasicLayout.defaultProps = {
  children: null,
  login: null,
}

export { BasicLayout as BasicLayoutComponent }

export default BasicLayout