import pages from '../pages'

import routes from './routeUrls'

const siteMap = [
  {
    path: routes.MAIN_PAGE,
    component: pages.BasicLayout,
    routes: [
      {
        component: pages.MarketsPage,
        path: routes.MAIN_PAGE,
      },
      // {
      //   component: pages.UsersPage,
      //   path: routing.routes.PAGE_USERS,
      // },
      // {
      //   component: pages.CreatePaymentPage,
      //   path: routing.routes.PAGE_CREATE_PAYMENT,
      // },
      // {
      //   component: pages.BankPage,
      //   path: routing.routes.PAGE_BANK,
      // },
      // {
      //   component: pages.BanksPage,
      //   path: routing.routes.PAGE_BANKS,
      // },
    ],
  },
]

export default siteMap
