import { all } from 'redux-saga/effects';
import { sagaMain } from 'base-front-ts';
import sagaCommon from './common';
//
export default function* mainSaga() {
  yield all([sagaMain(), sagaCommon()]);
}
