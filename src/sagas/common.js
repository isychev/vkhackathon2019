import { put, all, takeEvery } from 'redux-saga/effects';
import { onRedirect } from 'base-front-ts';
import { UNAUTHORIZED } from 'base-front-ts/constantsRedux';

import routing from '../routing';

function* sagaUnauthRedirect() {
  yield put(onRedirect(routing.generate(routing.PATHS.PAGE_LOGIN)));
}

export default function* mainSaga() {
  yield all([takeEvery(UNAUTHORIZED, sagaUnauthRedirect)]);
}
