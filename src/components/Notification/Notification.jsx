import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
  selectorLocalTempNotification,
  onRemoveNotification as onRemoveNotificationAction,
} from 'base-front-ts';
import { NOTIFICATION_TIME } from '../../appConstants';
import './notification.scss';

// компонент вывода сообщений в верхней части сайта
// все сообщения выводятся через него

const NotificationComponent = class extends Component {
  static propTypes = {
    notifications: PropTypes.arrayOf(
      PropTypes.shape({
        text: PropTypes.string,
        type: PropTypes.string,
        lock: PropTypes.bool,
        showLoading: PropTypes.bool,
      }),
    ),
    onRemoveNotification: PropTypes.func,
  };

  static defaultProps = {
    notifications: [],
    onRemoveNotification: f => f,
  };

  state = {
    notifications: [],
  };

  componentWillReceiveProps(nextProps) {
    this.setState({
      notifications: [
        ...nextProps.notifications.map(notification => ({
          ...notification,
          timeoutId:
            !notification.showLoading && !notification.lock
              ? setTimeout(
                  () => this.handleClick(notification),
                  NOTIFICATION_TIME,
                )
              : 0,
        })),
      ],
    });
  }

  // setNotification = notification => {
  //   this.setState({
  //     notifications: [
  //       ...this.state.notifications.map(curentNotification => {
  //         if (curentNotification.id === notification) {
  //           return {
  //             ...notification,
  //             timeoutId:
  //               !notification.showLoading && !notification.lock
  //                 ? setTimeout(
  //                     () => this.handleClick(notification),
  //                     NOTIFICATION_TIME,
  //                   )
  //                 : 0,
  //           };
  //         }
  //       }),
  //     ],
  //   });
  // };

  // removeTimeoutClose = ({ id }) => {
  //   this.setState({
  //     notifications: this.state.notifications.filter(
  //       notification => notification.id !== text,
  //     ),
  //   });
  // };

  // ручное закрытие сообщения
  handleClick = notification => {
    // this.removeTimeoutClose(notification);
    if (this.props.onRemoveNotification) {
      this.props.onRemoveNotification(notification);
    }
  };

  render() {
    const { notifications } = this.state;
    if (!notifications.length) {
      return null;
    }
    return (
      <div className="alert-fixed ">
        {notifications.map(
          ({ id, text, type = 'success', showLoading, lock }) => (
            <div key={id || text} className={`alert alert-${type} fadeIn mb-2`}>
              <div className="d-flex d-flex justify-content-between align-items-start">
                <div className="d-inline-block text-left">
                  <span>{text}</span>
                  {showLoading ? (
                    <span className="icon-base icon-loading px-2 py-2 d-inline-block align-bottom ml-2" />
                  ) : null}
                </div>
                {!lock ? (
                  <button
                    type="button"
                    className="btn-reset icon-close px-2 py-2 float-right"
                    onClick={() => this.handleClick({ text })}
                  />
                ) : null}
              </div>
            </div>
          ),
        )}
      </div>
    );
  }
};
export { NotificationComponent };

export default connect(
  selectorLocalTempNotification,
  {
    onRemoveNotification: onRemoveNotificationAction,
  },
)(NotificationComponent);
