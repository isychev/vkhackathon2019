import {
  entityModuleName,
  listModuleName,
  reducerEntities,
  reducerList,
  reducerRouting,
  reducerTemp,
  routingModuleName,
  sagaRedirect,
  tempModuleName,
} from 'base-front-ts'
import React, { Component } from 'react'
import { Provider } from 'react-redux'
import { ConnectedRouter } from 'react-router-redux'
import { all, put, take } from 'redux-saga/effects'
import './App.scss'

import sagaModal from 'base-front-ts/sagaModal'
import axiosSaga from 'base-front-ts/sagaRequest'
import storeFactory from 'base-front-ts/createStore'
import Notification from './components/Notification'
import siteMap from './config/siteMap'
import RouterRenderer from './routers/RouterRenderer'
import axios from 'axios'

function * mainSaga () {
  yield all([
    axiosSaga(axios),
    sagaRedirect(),
    sagaModal(),
  ])
}

const reducer = {
  [entityModuleName]: reducerEntities,
  [listModuleName]: reducerList,
  [routingModuleName]: reducerRouting,
  [tempModuleName]: reducerTemp,
}

class App extends Component {
  constructor (props) {
    super(props)
    this.store = storeFactory({reducer, saga: mainSaga})
    console.log('this.store', this.store)
  }

  render () {
    return (
      <Provider store={this.store}>
        <ConnectedRouter store={this.store} history={this.store.history}>
            <RouterRenderer siteMap={siteMap}/>
        </ConnectedRouter>
      </Provider>
    )
  }
}

export default App
