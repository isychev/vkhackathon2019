import Routing from "base-front-ts/routing";
import routeUrls from "./config/routeUrls";

const routing = new Routing(routeUrls);

export default routing;
