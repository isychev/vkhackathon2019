import React from "react";
import { Route } from "react-router-dom";


const RouteWithSubRoutes  = ({
  route,
}) => (
  <Route
    path={route.path}
    render={(props) => {
      const RenderComponent = route.component;
      return (
        <RenderComponent {...props} routes={route.routes} route={route} />
      );
    }}
  />
);

export default RouteWithSubRoutes;
