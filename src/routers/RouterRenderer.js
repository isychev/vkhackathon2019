import * as React from "react";
import { Switch } from "react-router-dom";
import RouteWithSubRoutes from "./RouteWithSubRoutes";


const RouterRenderer = ({
  siteMap,
}) => (
  <Switch>
    {siteMap.map((route) => (
      <RouteWithSubRoutes
        {...route}
        route={route}
        key={Array.isArray(route.path) ? route.path[0] : route.path}
      />
    ))}
  </Switch>
);

export default RouterRenderer;
