var express = require('express');
var cors = require('cors')
var app = express();

app.use(cors())
app.use(express.static('build'));

app.get('/api', function (req, res) {
  res.send('Hello World vk!');
});

app.listen(process.env.PORT || 3000, function () {
  console.log('Example app listening on port 3000 !');
});